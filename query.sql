/**
  Posibles valores de "status":
  - pending - pendiente de análisis
  - approved - aprobado para subir a BD
  - loading - analizando datos
  - reject - rechazado
  - finalized - subido a la BD
 */
drop TABLE document;
create table document
(
  id int auto_increment
    primary key,
  type varchar(100) null,
  original_name varchar(150) null,
  path varchar(200) null,
  status varchar(20) null,
  error text null,
  createdAt datetime default CURRENT_TIMESTAMP null,
  entity int null,
  year int null,
  month int null
);

create index document_status_index
  on document (status);