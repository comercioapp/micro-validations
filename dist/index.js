'use strict';

const {json, send} = require('micro');
const micro = require('micro')
const {upload,move} = require('micro-upload');
const uuidV4 = require('uuid/v4');
const pathLib = require('path');

const XLSX = require('xlsx');

const rules = require('./rules.json');
const validations = require('./lib/validations');
const mysql = require('./lib/mysql');

const server = micro(upload((req, res) => __async(function*(){

    let file = req.files.file;
    let documentType = req.body.type;
    if(file === undefined || documentType === undefined){
        send(res, 400, 'parámetros incorrectos')
        return;
    }

    let entity = req.body.entity
    let year = req.body.year
    let month = req.body.month
    let originalName = file.name;
    let extension = pathLib.extname(originalName)
    if(extension !== '.xls' && extension !== '.xlsx' ){
        send(res, 400, 'formato incorrecto')
        return;
    }
    let path = __dirname + '/files/' + uuidV4() + extension;
    file.mv(path, function () {return __async(function*(){

        const documentId = yield mysql.insertDocument(originalName,path,documentType,entity,year,month);
        res.end('Documento subido exitosamente.');

        let result = {};
        let workbook = XLSX.readFile(path,{type:'file'});
        workbook.SheetNames.forEach( function(sheetName) {return __async(function*(){
            let excelRange = XLSX.utils.decode_range(workbook.Sheets[sheetName]['!ref']);
            let fileRules = rules[documentType]['validations'];
            console.log('Hoja: ' + sheetName);
            let success = true;
            for (let rul of fileRules) {
                let ruleRange = rul.range.replace("*", (excelRange.e.r + 1));
                let ruleRangeDecode = XLSX.utils.decode_range(ruleRange);
                let ruleAction = rul.action;
                console.log('Rango: ' + ruleRange);
                console.log('Regla: ' + ruleAction);

                for (var i = ruleRangeDecode.s.c; i <= ruleRangeDecode.e.c; i++) {
                    for (var j = ruleRangeDecode.s.r; j <= ruleRangeDecode.e.r; j++) {
                        let cell = XLSX.utils.encode_cell({c: i, r: j});
                        let cellValue = (workbook.Sheets[sheetName][cell]) ? workbook.Sheets[sheetName][cell].v : ''
                        try {
                            yield validations[ruleAction](cellValue,cell,workbook.Sheets[sheetName]);
                        } catch (err) {
                            console.log(err);
                            yield mysql.updateDocumentError(documentId,err);
                            success = false;
                        }
                    }
                }
            }
            if (success)
                yield mysql.updateDocumentSuccess(documentId);
            return;
        }())});
    }())});
}())));

server.listen(3000);

function __async(g){return new Promise(function(s,j){function c(a,x){try{var r=g[x?"throw":"next"](a)}catch(e){j(e);return}r.done?s(r.value):Promise.resolve(r.value).then(c,d)}function d(e){c(e,1)}c()})}
