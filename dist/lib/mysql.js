let mysqlDriver = require('mysql2/promise');
let connectionData = {
    host     : 'localhost',
    user     : 'root',
    password : 'admin',
    database : 'perc_micro'
};

let mysql = {
    runQuery(query,params){return __async(function*(){
        const connection = yield mysqlDriver.createConnection(connectionData);
        const [rows, fields] = yield connection.execute(query,params);
        yield connection.end();
        return rows;
    }())},

    insertDocument(name,path,type,entity,year,month){return __async(function*(){
        console.log(name,path,type,entity,year,month)
        const connection = yield mysqlDriver.createConnection(connectionData);
        yield connection.execute(
            "INSERT INTO document (type,original_name,path,status,entity,year,month) VALUES (?,?,?,'loading',?,?,?);",[type,name,path,entity,year,month]
        );
        const [rows, fields] = yield connection.execute(
            "SELECT MAX(id) as 'id' FROM document;"
        );
        yield connection.end();
        return rows[0].id;
    }())},

    updateDocumentError(id,error){return __async(function*(){
        const connection = yield mysqlDriver.createConnection(connectionData);
        yield connection.execute(
            "UPDATE document SET error = IFNULL(CONCAT( error , '|', ?), ?) ,status = 'reject' WHERE id = ? ;",[error,error,id]
        );
        yield connection.end();
    }())},

    updateDocumentSuccess(id){return __async(function*(){
        const connection = yield mysqlDriver.createConnection(connectionData);
        yield connection.execute(
            "UPDATE document SET status = 'approved' WHERE id = ? ;",[id]
        );
        yield connection.end();
    }())},
};

module.exports = mysql;
function __async(g){return new Promise(function(s,j){function c(a,x){try{var r=g[x?"throw":"next"](a)}catch(e){j(e);return}r.done?s(r.value):Promise.resolve(r.value).then(c,d)}function d(e){c(e,1)}c()})}
