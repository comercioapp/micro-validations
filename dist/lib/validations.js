let mysql = require('./mysql');
let _ = require('lodash');

let validations = {
    notBlank(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined)
            throw "Celda: " + cell + ",Error: no puede estar vacio";
        return true;
    }())},
    isNumber(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined)
            return true;
        if(!(isFinite(parseFloat(value))))
            throw "Celda: "+cell+",Valor: "+value+", Error: debe ser un valor entero";
        return true;
    }())},
    isInteger(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined)
            return true;
        if(value == _.toInteger(value))
            return true
        throw "Celda: "+cell+",Valor: "+value+", Error: debe ser un valor entero";
    }())},
    // centros_de_produccion
    customCP(value,cell,workbook){return __async(function*(){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entity_cost_centers WHERE cost_center_id = "+ value +" ;");
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    // distribucion_suministro
    customDM(value,cell,workbook){return __async(function*(){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    customDM2(value,cell,workbook){return __async(function*(){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entities_supplies ON entities.id = entities_supplies.entity_id WHERE entities.id = 1 AND entities_supplies.supply_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    isUnique(value,cell,workbook){return __async(function*(){
        return true;
    }())},
    CustomEmp(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined)
            throw "Celda: " + cell + ",Error: no puede estar vacio";
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities_staffs INNER JOIN staffs ON entities_staffs.staff_id = staffs.id WHERE  entities_staffs.entity_id = 1 AND staffs.code = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    CustomEmp2(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined)
            return true;
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM labor_standards WHERE geography_id = 83 AND code = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    CustomEmp3(value,cell,workbook){return __async(function*(){
        if(value==="1" || value==="2")
            return true;
        throw "Celda: "+cell+",Valor: "+value+", Error: valor debe estar entre 1 y 2";
    }())},
    customGG(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
    customGG2(value,cell,workbook){return __async(function*(){
        // TODO: Puede estar vacio?
        // await this.notBlank(value,cell,workbook);

        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entities_supplies ON entities.id = entities_supplies.entity_id WHERE entities.id = 1 AND entities_supplies.supply_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }())},
    customGG3(value,cell,workbook){return __async(function*(){
        if(value==="" || value===null || value === undefined || value === "0" || value === 0  )
            return true;
        if(value=="1" || value== "2" || value== "3" || value== "4" )
            return true;
        throw "Celda: "+cell+",Valor: "+value+", Error: valor debe estar entre 1 y 4";
    }())},
    customPS(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);

        value = value.split("__");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
    customPS2(value,cell,workbook){return __async(function*(){
        return true;
    }())},
    customPD(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);

        value = value.split("_");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
    customPD2(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
    customPD3(value,cell,workbook){return __async(function*(){
        yield this.isNumber(value,cell,workbook);
        return true;
    }.call(this))},
    customPH(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);
        value = value.split("__");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM payrolls WHERE id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
    customPH2(value,cell,workbook){return __async(function*(){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        yield this.isInteger(value,cell,workbook);
        return true;
    }.call(this))},
    customPH3(value,cell,workbook){return __async(function*(){
        yield this.notBlank(value,cell,workbook);
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = yield mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    }.call(this))},
};

module.exports = validations;
function __async(g){return new Promise(function(s,j){function c(a,x){try{var r=g[x?"throw":"next"](a)}catch(e){j(e);return}r.done?s(r.value):Promise.resolve(r.value).then(c,d)}function d(e){c(e,1)}c()})}
