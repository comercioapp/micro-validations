let mysqlDriver = require('mysql2/promise');
let connectionData = {
    host     : 'localhost',
    user     : 'root',
    password : 'admin',
    database : 'perc_micro'
};

let mysql = {
    async runQuery(query,params){
        const connection = await mysqlDriver.createConnection(connectionData);
        const [rows, fields] = await connection.execute(query,params);
        await connection.end();
        return rows;
    },

    async insertDocument(name,path,type,entity,year,month){
        const connection = await mysqlDriver.createConnection(connectionData);
        await connection.execute(
            "INSERT INTO document (type,original_name,path,status,entity,year,month) VALUES (?,?,?,'loading',?,?,?);",[type,name,path,entity,year,month]
        );
        const [rows, fields] = await connection.execute(
            "SELECT MAX(id) as 'id' FROM document;"
        );
        await connection.end();
        return rows[0].id;
    },

    async updateDocumentError(id,error){
        const connection = await mysqlDriver.createConnection(connectionData);
        await connection.execute(
            "UPDATE document SET error = IFNULL(CONCAT( error , '|', ?), ?) ,status = 'reject' WHERE id = ? ;",[error,error,id]
        );
        await connection.end();
    },

    async updateDocumentSuccess(id){
        const connection = await mysqlDriver.createConnection(connectionData);
        await connection.execute(
            "UPDATE document SET status = 'approved' WHERE id = ? ;",[id]
        );
        await connection.end();
    },
};

module.exports = mysql;