let mysql = require('./mysql');
let _ = require('lodash');

let validations = {
    async notBlank(value,cell,workbook){
        if(value==="" || value===null || value === undefined)
            throw "Celda: " + cell + ",Error: no puede estar vacio";
        return true;
    },
    async isNumber(value,cell,workbook){
        if(value==="" || value===null || value === undefined)
            return true;
        if(!(isFinite(parseFloat(value))))
            throw "Celda: "+cell+",Valor: "+value+", Error: debe ser un valor entero";
        return true;
    },
    async isInteger(value,cell,workbook){
        if(value==="" || value===null || value === undefined)
            return true;
        if(value == _.toInteger(value))
            return true
        throw "Celda: "+cell+",Valor: "+value+", Error: debe ser un valor entero";
    },
    // centros_de_produccion
    async customCP(value,cell,workbook){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entity_cost_centers WHERE cost_center_id = "+ value +" ;");
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    // distribucion_suministro
    async customDM(value,cell,workbook){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customDM2(value,cell,workbook){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entities_supplies ON entities.id = entities_supplies.entity_id WHERE entities.id = 1 AND entities_supplies.supply_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async isUnique(value,cell,workbook){
        return true;
    },
    async CustomEmp(value,cell,workbook){
        if(value==="" || value===null || value === undefined)
            throw "Celda: " + cell + ",Error: no puede estar vacio";
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities_staffs INNER JOIN staffs ON entities_staffs.staff_id = staffs.id WHERE  entities_staffs.entity_id = 1 AND staffs.code = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async CustomEmp2(value,cell,workbook){
        if(value==="" || value===null || value === undefined)
            return true;
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM labor_standards WHERE geography_id = 83 AND code = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async CustomEmp3(value,cell,workbook){
        if(value==="1" || value==="2")
            return true;
        throw "Celda: "+cell+",Valor: "+value+", Error: valor debe estar entre 1 y 2";
    },
    async customGG(value,cell,workbook){
        await this.notBlank(value,cell,workbook);
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customGG2(value,cell,workbook){
        // TODO: Puede estar vacio?
        // await this.notBlank(value,cell,workbook);

        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entities_supplies ON entities.id = entities_supplies.entity_id WHERE entities.id = 1 AND entities_supplies.supply_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customGG3(value,cell,workbook){
        if(value==="" || value===null || value === undefined || value === "0" || value === 0  )
            return true;
        if(value=="1" || value== "2" || value== "3" || value== "4" )
            return true;
        throw "Celda: "+cell+",Valor: "+value+", Error: valor debe estar entre 1 y 4";
    },
    async customPS(value,cell,workbook){
        await this.notBlank(value,cell,workbook);

        value = value.split("__");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customPS2(value,cell,workbook){
        return true;
    },
    async customPD(value,cell,workbook){
        await this.notBlank(value,cell,workbook);

        value = value.split("_");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customPD2(value,cell,workbook){
        await this.notBlank(value,cell,workbook);
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];

        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customPD3(value,cell,workbook){
        await this.isNumber(value,cell,workbook);
        return true;
    },
    async customPH(value,cell,workbook){
        await this.notBlank(value,cell,workbook);
        value = value.split("__");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM payrolls WHERE id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
    async customPH2(value,cell,workbook){
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        await this.isInteger(value,cell,workbook);
        return true;
    },
    async customPH3(value,cell,workbook){
        await this.notBlank(value,cell,workbook);
        value = value.split("-");
        if(value.length < 2)
            throw "Celda: "+cell+",Valor: "+value+", Error: valor inválido";
        value = value[0];
        let resultQuery = await mysql.runQuery("SELECT COUNT(*) as 'count' FROM entities INNER JOIN entity_cost_centers ON entities.id = entity_cost_centers.entity_id WHERE entities.id = 1 AND entity_cost_centers.cost_center_id = ?",[value]);
        if( parseInt(resultQuery[0].count) === 0 )
            throw "Celda: "+cell+",Valor: "+value+", Error: no existe en la BD";
        return true;
    },
};

module.exports = validations;