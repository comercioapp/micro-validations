##### Ejecutar los siguientes comandos:

`npm install` 

`npm run compile`

##### Instalar [forever](https://github.com/foreverjs/forever) para correr NodeJs como demonio:

`npm install forever -g`


Iniciar el servicio:

`forever start dist/index.js`

Listar los sericios:

`forever list`

